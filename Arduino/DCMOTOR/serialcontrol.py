#/usr/bin/env python
# Programa de prueba del Arduino con Python y pygame
# No es necesario para el proyecto final pero fue util para su creacion


import os
import pygame
import serial

def pygameSetup():
    pygame.init()
    screen = pygame.display.set_mode((468, 60))

    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((250, 250, 250))

    if pygame.font:
        font = pygame.font.Font(None, 36)
        text = font.render("Control your car with Python!", 1, (10, 10, 10))
        textpos = text.get_rect(centerx=background.get_width()/2)
        background.blit(text, textpos)

    screen.blit(background, (0, 0))
    pygame.display.flip()


def main():

    pygameSetup()

    ser = serial.Serial('/dev/ttyACM0', 9600)

    clock = pygame.time.Clock()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
            elif event.type == pygame.KEYDOWN:
                ser.write(bytes(chr(event.key), 'UTF-8'))
            elif event.type == pygame.KEYUP:
                ser.write(bytes(chr(0), 'UTF-8'))


if __name__ == '__main__':
    main()
