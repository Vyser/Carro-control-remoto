// Programa a correr en el Arduino

// Se deben conectar los pines segun el Motor Shield que se tenga
// Motores de la Izquierda
int enA = 10;
int in1 = 9;
int in2 = 8;
// Motores de la Derecha
int enB = 11;
int in3 = 12;
int in4 = 13;

int val;

void setup()
{
  // Todos los pines deben de estar en OUTPUT
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  
  Serial.begin(9600); // El baud-rate se puede modificar
}

// Funciones que le envia la senal a los motores para su movimiento
void adelante()
{
 
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  
  analogWrite(enA, 255);

  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  
  analogWrite(enB, 255);
//  delay(300);
  
}

void adelante_lento()
{
 
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  
  analogWrite(enA, 145); // Velocidad

  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  
  analogWrite(enB, 145);
//  delay(300);
  
}  

void atras(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  
  analogWrite(enA, 255);

  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  
  analogWrite(enB, 255);
//  delay(300);
}

void atras_lento()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  
  analogWrite(enA, 145);

  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  
  analogWrite(enB, 145);
  //delay(300);
}

void derecha_adelante(){
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  
  analogWrite(enA, 255);

  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  
  analogWrite(enB, 255);
//  delay(300);
}

void derecha_atras(){
  digitalWrite(in2, HIGH);
  digitalWrite(in1, LOW);
  
  analogWrite(enA, 255);

  digitalWrite(in4, LOW);
  digitalWrite(in3, LOW);
  
  analogWrite(enB, 255);
//  delay(300);
}

void izquierda_adelante(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  
  analogWrite(enA, 255);

  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  
  analogWrite(enB, 255);
//  delay(300);
}

void izquierda_atras(){
  digitalWrite(in2, LOW);
  digitalWrite(in1, LOW);
  
  analogWrite(enA, 255);

  digitalWrite(in4, HIGH);
  digitalWrite(in3, LOW);
  
  analogWrite(enB, 255);
//  delay(300);
}

void loop()
{
  if (Serial.available()) {
    val = Serial.read();
    
    // Event manager
    switch(val){
    case 'w':
      adelante();
      break;
    case 's':
      atras();
      break;
    case 'd':
      derecha_adelante();
      break;
    case 'a':
      izquierda_adelante();
      break;
    case 'i':
     adelante_lento();
     break;
    case 'k':
     atras_lento();
     break;
    case 'l':
     derecha_atras();
     break;
    case 'j':
     izquierda_atras();
     break;
     
    default: // Si recibe cualquier otro char el carro estara frenado
      digitalWrite(in1, LOW);
      digitalWrite(in2, LOW);  
      digitalWrite(in3, LOW);
      digitalWrite(in4, LOW);

    }
  }
}
