# Carro a Control Remoto

Proyecto del curso Programación bajo Plataformas Abiertas de la Universidad de Costa Rica. 
Profesor Ing. Andrés Mora Zúñiga

Integrantes:
* Gabriel Jimenez Amador B73895
* Victor Manuel Yeom Song B78494
* Diego Matamoros Arnáez B74566
* Luis Guillermo Ramirez B76222

Constituye en un carro controlado a distancia mediante el acelerómetro de una 
laptop con ayuda de hardware y software *Open Source*. 
En el que incluye Linux, Python 2.7 y el software de Arduino.



## Requisitos

Se deben instalar módulos para Python 2.7 como

* kivy
* sockets
* pySerial
* pygame
* picamera
* io
* threading

## Funcionamiento
Su funcionamiento se puede ver en el siguiente video:

<a href="http://www.youtube.com/watch?feature=player_embedded&v=z5cPaljJ-Sk
" target="_blank"><img src="http://img.youtube.com/vi/z5cPaljJ-Sk/0.jpg" 
alt="Carro a Control Remoto" width="240" height="180" border="10" /></a>