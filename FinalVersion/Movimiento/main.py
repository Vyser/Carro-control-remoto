#!/usr/bin/env python
# -*- coding: utf-8 -*-

from kivy.app import App #Para hacer una aplicación
from plyer import accelerometer #Obtener la información del acelerómetro
from kivy.clock import Clock #Establecer intervalos para enviar instrucciones
from kivy.uix.floatlayout import FloatLayout #Interfaz de usuario
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('192.168.1.2', 18000))

class UI(BoxLayout):
    def __init__(self, **kwargs):
        super(UI, self).__init__(**kwargs)

        self.main_layout = BoxLayout(orientation="vertical")

        layout1 = BoxLayout()
        txt = ""
        layout2 = BoxLayout(size_hint=(1, .5))
        self.lblAcce = Label(text="Accelerometer: ")

        # Deja de usar el acelerometro para usar los botones
        def callback1(instance):
            Clock.unschedule(self.update)
            Clock.schedule_once(callback5) # Llama al callback una sola vez
        def callback2(instance):
            Clock.unschedule(self.update)
            Clock.schedule_once(callback7)
        def callback3(instance):
            Clock.unschedule(self.update)
            Clock.schedule_once(callback9)
        def callback4(instance):
            Clock.unschedule(self.update)
            Clock.schedule_once(callback11)

        # Se avanza y upon release deja de avanzar
        def callback5(instance):
            txt = "Avanzar"
            self.lblAcce.text = txt
            s.send("w")

        # Se va a la izquierda y upon release deja de ir
        def callback7(instance):
            txt = "Izquierda"
            self.lblAcce.text = txt
            s.send("a")

        # Se va a la derecha y upon release deja de ir
        def callback9(instance):
            txt = "Derecha"
            self.lblAcce.text = txt
            s.send("d")

        # Se retrocede y upon release deja de retroceder
        def callback11(instance):
            txt = "Retroceder"
            self.lblAcce.text = txt
            s.send("s")

        # Cancela enviar datos
        def callback0(instance):
            s.send(chr(0)) # Upon release manda Null
            Clock.schedule_interval(self.update, 1.0/24)


        # Revisa las acciones del usuario con los botones
        btn1 = Button(text='Avanzar',background_color=(1,1,1,.3))
        btn1.bind(on_press=callback1)
        btn1.bind(on_release=callback0)

        btn2 = Button(text='Izquierda',background_color=(1,1,1,.3))
        btn2.bind(on_press=callback2)
        btn2.bind(on_release=callback0)

        btn3 = Button(text='Derecha',background_color=(1,1,1,.3))
        btn3.bind(on_press=callback3)
        btn3.bind(on_release=callback0)

        btn4 = Button(text='Retroceder',background_color=(1,1,1,.3))
        btn4.bind(on_press=callback4)
        btn4.bind(on_release=callback0)

        # Agrega los botones
        layout2.add_widget(btn1)
        layout2.add_widget(btn2)
        layout2.add_widget(btn3)
        layout2.add_widget(btn4)
        layout1.add_widget(self.lblAcce)
        self.main_layout.add_widget(layout1)
        self.main_layout.add_widget(layout2)

        self.add_widget(self.main_layout)

        # Intenta iniciar el acelerometro
        try:
            accelerometer.enable() # inicia el acelerometro

            Clock.schedule_interval(self.update, 1.0/24) #24 calls per second
        except:
            self.lblAcce.text = "No se pudo iniciar el acelerometro!" #error


    # Función para montitorear el acelerometro
    def update(self, dt):
        try:
            if accelerometer.acceleration[0]<-2 and accelerometer.acceleration[0]>-4:
                s.send("a") # Izquierda
            if accelerometer.acceleration[0]>2 and accelerometer.acceleration[0]<4:
                s.send("d") # Derecha
            if accelerometer.acceleration[1]<-4 and accelerometer.acceleration[1]>-6:
                s.send("s") # Atras
            if accelerometer.acceleration[1]<6 and accelerometer.acceleration[1]>4:
                s.send("w") # Adelante
            if accelerometer.acceleration[1]<-2 and accelerometer.acceleration[1]>-4:
                s.send("k") # Atras lento
            if accelerometer.acceleration[1]>2 and accelerometer.acceleration[1]<4:
                s.send("i") # Adelante lento
            if accelerometer.acceleration[1]<2 and accelerometer.acceleration[1]>-2 and accelerometer.acceleration[0]<2 and accelerometer.acceleration[0]>-2:
                txt = "Sin Movimiento"
                s.send(chr(0))

        except:
            txt = "No se puede leer el acelerómetro!" #error
            self.lblAcce.text = txt # Agrega el texto correcto


class Accelerometer(App):
    def build(self):
        ui = UI()
        return ui

if __name__ == '__main__':
    Accelerometer().run()
