#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import sys
import serial
from threading import *
import pygame
import pygame.camera
from pygame.locals import *

ser = serial.Serial('/dev/ttyACM0', 9600) # Puerto por default, con baud de 9600

HOST = '192.168.1.2' # Localhost
PORT = 18000 # Puerto de conexión al socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Socket estándar
print 'Socket inicializado ...\n'
try:
    s.bind((HOST, PORT)) # Fija la conexión del servidor
except socket.error as err:
    print 'No se pudo realizar la conexión, la aplicación se cerrará'
    sys.exit()
print '¡La conexión fue exitosa!\n'
s.listen(10) # Busca solicitudes de conexión al server
conn, addr = s.accept() # Acepta la solicitud de conexión de un cliente
print 'Conectado a ' + addr[0] + ': ' + str(addr[1]) + '\n'
msg = ""
msg = conn.recv(1000) # Recibe los mensajes que envía el cliente

a = 1
while a == 1:
    msg = conn.recv(1000)
    ser.write(bytes(msg.encode('utf-8'))) # Escribe los datos que se envían al cliente al puerto

    # Define mensaje a imprimir según la señal que recibe del cliente
    if msg == "w":
        msg = "Adelante"
    elif msg == "a":
        msg = "Izquierda"
    elif msg == "d":
        msg = "Derecha"
    elif msg == "s":
        msg = "Reversa"
    elif msg == "i":
        msg = "Adelante lento"
    elif msg == "j":
        msg = "Atras lento"

    print(msg)


s.close() # Cierra la conexión del cliente
