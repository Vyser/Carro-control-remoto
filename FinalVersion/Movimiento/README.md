## Cliente y servidor del movimiento

*main.py* se corre en el ordenador mientras que *server.py* en el Raspberry Pi.

Se debe modificar la dirección IP local del Raspberry Pi para realizar la conexión 
mediante **sockets**, esta se puede conseguir con **ifconfig** en la terminal.