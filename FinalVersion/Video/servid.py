import picamera
import pygame
import io
import socket

# Init es inicialización
# Init pygame
pygame.init()
screen = pygame.display.set_mode((0,0))

# Init camera
camera = picamera.PiCamera()
camera.resolution = (640, 480) # Define resolución de la imagen
camera.crop = (0.0, 0.0, 1.0, 1.0)
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Socket estándar
serversocket.bind(("192.168.1.2",18000)) # Fija la dirección del socket al IP
serversocket.listen(10) # Busca solicitudes de conexión al servidor

# Dimensiones de la pantalla
x = (screen.get_width() - camera.resolution[0]) / 2
y = (screen.get_height() - camera.resolution[1]) / 2

# Init buffer
rgb = bytearray(camera.resolution[0] * camera.resolution[1] * 3)

# Main loop
exitFlag = True
while(exitFlag): # Loop que revisa si está grabando
    for event in pygame.event.get():
        if(event.type is pygame.MOUSEBUTTONDOWN or
           event.type is pygame.QUIT): # Define la condición de salida de la aplicación
            exitFlag = False
    connection, adress= serversocket.accept() # Acepta la solicitud de conexión del cliente
    stream = io.BytesIO() # Crea un buffer
    camera.capture(stream, use_video_port=True, format='rgb') # Toma una imagen
    stream.seek(0) # Busca la posición del stream en el array que sirve como buffer
    stream.readinto(rgb) # Lee los bytes que conforman una imagen
    stream.close() # Cierra el stream
    img = pygame.image.frombuffer(rgb[0:
          (camera.resolution[0] * camera.resolution[1] * 3)],
           camera.resolution, 'RGB')
    # Se lee desde el buffer y guarda la imagen en formato RGB
    screen.fill(0)
    if img:
        screen.blit(img, (x,y))# Se despliega la imagen en la pantalla
        data = pygame.image.tostring(img,"RGB") # Convierte de imagen a String
    	connection.sendall(data) # Envía los datos de imagen al socket
    	connection.close() # Cierra la conexión

    pygame.display.update() # Refresca la pantalla

camera.close()
pygame.display.quit()
