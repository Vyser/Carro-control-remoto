from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.clock import Clock
import socket
from threading import *
from kivy.uix.image import Image
import pygame
from kivy.uix.label import Label
from kivy.uix.button import Button

# Interfaz gráfica de la aplicación
kv = '''
main:
	BoxLayout:
		orientation: 'vertical'
		padding: root.width * 0.05, root.height * .05
		spacing: '5dp'
		BoxLayout:
			size_hint: [1,.85]
			Image:
				id: image_source
				source: 'foo.jpg'
		BoxLayout:
			size_hint: [1,.15]
			GridLayout:
				cols: 3
				spacing: '10dp'
				Button:
					text:'Play'
					on_press: root.playPause()

'''

class main(BoxLayout):
	ipAddress = "192.168.1.2" # Dirección IP del servidor
	port = 18000 # Puerto asociado al socket que recibe al cliente

	# Define el intervalo de tiempo entre cada frame
	def playPause(self):
		Clock.schedule_interval(self.recv, 0.1)

	# Función que recibe los datos del stream de video
	def recv(self, dt):
		clientsocket=socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Socket estándar
		clientsocket.connect((self.ipAddress, self.port)) # Conecta el cliente al servidor
		received = [] # Lista donde se guardan las imágenes recibidas del servidor
		while True: # Loop para mantener el stream por tiempo indefinido
			recvd_data = clientsocket.recv(230400) # Recibe datos del socket y los convierte a String
			if not recvd_data: # Si no se reciben imágenes se cierra el loop
				break
			else:
				received.append(recvd_data) # Mientras se reciben imágenes las guarda
		dataset = ''.join(received)#une el array en una sola string
		image = pygame.image.fromstring(dataset,(640, 480),"RGB") # Conversión de String a imagen
		try:
			pygame.image.save(image, "foo.jpg") # Inicializa el streaming de video
			self.ids.image_source.reload() # Refresca la imagen en pantalla
		except:
			pass # Si no hay conexión cierra la aplicación


class videoStreamApp(App): # Ejecuta la app
    def build(self):
        return Builder.load_string(kv)

videoStreamApp().run()
